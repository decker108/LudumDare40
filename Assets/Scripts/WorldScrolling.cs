﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldScrolling : MonoBehaviour {

    MainHandler mainHandler;
    public bool overrideSpeed = false;
    public float localScrollSpeed;
    float spawnCount;

    void Start()
    {
        mainHandler = GameObject.FindGameObjectWithTag("MainHandler").GetComponent<MainHandler>();
    }
    // Update is called once per frame
    void Update ()
    {
        if(!overrideSpeed)
        {
            transform.position += new Vector3(0, 0, 1) * (mainHandler.scrollSpeed * Time.deltaTime);
        }
        else
        {
            transform.position += new Vector3(0, 0, 1) * (localScrollSpeed * Time.deltaTime);
        }

	}
}
