﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    MainHandler mainHandler;
    Vector3 addedPos;
    float addedHoriz;
    float addedVert;
    public float mvmtSpeed;

    void Start()
    {
        mainHandler = GameObject.FindGameObjectWithTag("MainHandler").GetComponent<MainHandler>();
    }

    // Update is called once per frame
    void Update()
    {
        CheckBorders();
        transform.position += addedPos * Time.deltaTime;
    }

    void CheckBorders()
    {
        RaycastHit hit;

        if(Physics.Raycast(transform.position, new Vector3(Input.GetAxisRaw("Horizontal"), 0,0), out hit, 1))
        {
            if(hit.transform.gameObject.tag == "Border")
            {
                addedHoriz = 0;
            }
            else
            {
                addedHoriz = Input.GetAxisRaw("Horizontal") * mvmtSpeed;
            }
        }
        else
        {
            addedHoriz = Input.GetAxisRaw("Horizontal") * mvmtSpeed;
        }
        if (Physics.Raycast(transform.position, new Vector3(0, 0, Input.GetAxisRaw("Vertical")), out hit, 1))
        {
            if (hit.transform.gameObject.tag == "Border")
            {
                addedVert = 0;
            }
            else
            {
                addedVert = Input.GetAxisRaw("Vertical") * mvmtSpeed;
            }
        }
        else
        {
            addedVert = Input.GetAxisRaw("Vertical") * mvmtSpeed;
        }
        addedPos = new Vector3(addedHoriz, 0, addedVert + mainHandler.scrollSpeed);
    }
}
