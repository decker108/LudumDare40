﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player : MonoBehaviour {

  public GameObject playership;
  public int playerLives = 3;
  public bool invincible = false;
  public float invincibleTime;

  public Material normalMat;
  public Material blinkMat;
  bool mat = false;
  GameObject SecretObject;

  //	private AudioSource ljud;

  public List<Renderer> blinkingStuff;
  public List<GameObject> heartIcons;



  void Start()
  {
    playership = GameObject.FindGameObjectWithTag ("Player");
    SecretObject = GameObject.FindGameObjectWithTag ("MainHandler");
    //		ljud = this.gameObject.GetComponent<AudioSource> ();
  }


  public bool IsInvincible() {
    return invincible;
  }

  /*
  void OnTriggerEnter(Collider other)
  {
    if (!invincible && other.gameObject.tag == "Enemy") {
      TakeDamage();
    }
  }
  */

  public void TakeDamage() {
    playerLives--;
    DisplayLives ();
    invincible = true;
    invincibleTime = 3f;
    FlashingShip ();
    CheckIfDead ();
  }

  public void DisplayLives()
  {
    for (int i = heartIcons.Count; i > playerLives; i--)
    {
      heartIcons [i -1].SetActive (false);
    }
  }

  public void ResetGameState()
  {
    playerLives = 3;
    foreach(GameObject life in heartIcons)
    {
      life.SetActive (true);
    }
    SecretObject.GetComponent<MainHandler>().enemyKillCount = 0;

  }


  public void CheckIfDead()
  {
    if (playerLives <= 0)
    {
      SecretObject.GetComponent<MainHandler>().LoadGameOverPanel();

    }
  }


  void Update(){
    if (invincibleTime <= 0) {
      invincible = false;
    } else {
      invincibleTime -= Time.deltaTime;
    }
  }

  public void FlashingShip()
  {

    foreach(Renderer rend in blinkingStuff){
      rend.GetComponent<Renderer> ().enabled = mat;
    }
    mat = !mat;

    if (invincibleTime >= 0) {
      Invoke ("FlashingShip", 0.15f);
    } else 
    {
      foreach(Renderer rend in blinkingStuff)
      {
        rend.GetComponent<Renderer> ().enabled = true;
      }
    }
  }
}

