﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnWorld : MonoBehaviour {

  public GameObject worldChunk;
  public List<GameObject> enemySets;
  MainHandler mainHandler;
  public GameObject civilianBuilding;

  void Start()
  {
    mainHandler = GameObject.FindGameObjectWithTag ("MainHandler").GetComponent<MainHandler>();
  }

  void OnTriggerEnter(Collider other)
  {
    if(other.gameObject.tag == "Player")
    {
      Debug.Log("Spawn new chunk");
      GameObject chunk = GameObject.Instantiate(worldChunk, transform.parent.position, transform.parent.rotation);
      chunk.transform.position += new Vector3(0, 0, 100);


      //gör en for loop för hur många waves som ska spawna och sätt dem +x ifrån varandra.
      for(int i = 0; i <= mainHandler.numberOfWaves; i++)
      {
        int which = Random.Range(0,enemySets.Count);
        Vector3 pos = other.transform.position + new Vector3(0, 0, 100 + (i * 10));
        GameObject enemy = GameObject.Instantiate(enemySets[which], pos, other.transform.rotation);
      }
      for (int i = 0; i <= 5; i++)
      {
        GameObject civil = GameObject.Instantiate(civilianBuilding, other.transform.position, other.transform.rotation);
        civil.transform.position += new Vector3(Random.Range(-20, 20), 0, 100 + (Random.Range(-10, 20)));
      }

      GetComponent<Collider>().enabled = false;
    }
  }
}
