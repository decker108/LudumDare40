﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathMoverScript : MonoBehaviour {

	[SerializeField]
	public Transform[] target;
	public float speed;

	private int current;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		if (transform.position != target [current].position) {
			Vector3 pos = Vector3.MoveTowards (transform.position, target [current].position, speed * Time.deltaTime);
			GetComponent<Rigidbody> ().MovePosition (pos);

			// messes up above path finding
			Vector3 targetDir = target [current].position - transform.position;
			Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, -5 * Time.deltaTime, 0f);
			GetComponent<Rigidbody> ().MoveRotation (Quaternion.LookRotation (newDir));
		} else {
			if (current + 1 < target.Length) {
				current++;
			}
		}
	}
}
