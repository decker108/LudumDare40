﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDestruction : MonoBehaviour {

  GameObject SecretObject;
  public float timeToDeath = 20;
  [HideInInspector]
    public bool isDead;
  public bool rotate;

  // Use this for initialization
  void Start () {
    SecretObject = GameObject.FindGameObjectWithTag("MainHandler");
  }

  // Update is called once per frame
  void Update ()
  {

    timeToDeath -= Time.deltaTime;
    if (isDead == false && timeToDeath <= 0)
    {
      Death();
    }
    if (rotate == true)
    {
      transform.Rotate(Vector3.up * 360 * Time.deltaTime);
    }
  }


  public void OnTriggerEnter(Collider other)
  {
    if (other.gameObject.tag == "Bullet" && isDead == false)
    {
      SecretObject.GetComponent<MainHandler> ().enemyKillCount ++;
      Destroy(other.gameObject);
      Death();
    }

  }

  public void Death()
  {
    if(GetComponent<PathMoverScript>())
    {
      GetComponent<PathMoverScript>().enabled = false;
    }
    if(GetComponent<EnemyFixedAngleShooting>())
    {
      GetComponent<EnemyFixedAngleShooting>().dead = true;
    }
    isDead = true;
    rotate = true;
    this.transform.tag = "Wreck";
  }
}
