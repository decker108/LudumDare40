﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseBasedShooting : MonoBehaviour {

    Vector3 bulletDir;
    public GameObject bulletPrefab;
    public LayerMask layerMask;
	//public AudioClip BasicStartingShot;

    // Use this for initialization
    void Start () {
		
	}

	// Update is called once per frame
	void Update () {
        //bulletDir = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, transform.position.y, Input.mousePosition.z)) - transform.position;

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 100, layerMask))
        {
            if (hit.collider.gameObject.tag == "MouseLookPlane")
            {
                Vector3 hP = hit.point;
                bulletDir = new Vector3(hP.x, transform.position.y, hP.z);
            }
        }

        if (Input.GetMouseButtonDown(0))
        {
            Shoot();
        }
	}

	void OnEnable() {
		StartCoroutine (DeactivateAfterWait());
	}

    void Shoot()
    {
        GameObject newBullet = GameObject.Instantiate(bulletPrefab, transform.position, transform.rotation);
        newBullet.transform.LookAt(bulletDir);
		//AudioSource.PlayClipAtPoint (BasicStartingShot, Camera.main.transform.position);

    }

	IEnumerator DeactivateAfterWait()
	{
		yield return new WaitForSeconds(4);
		this.enabled = false;
	}
}
