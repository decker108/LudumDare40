﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnholyMess : MonoBehaviour {

  List<BodyShotPair> partsWithGuns;
  GameObject friendlyBullet;
  private float fireTime = 0;
  public float fireInterval;
  //	private AudioSource ljud;

  // Use this for initialization
  void Start () {
    partsWithGuns = new List<BodyShotPair>();
    //		ljud = this.gameObject.GetComponent<AudioSource> ();
    friendlyBullet = transform.parent.GetComponent<FixedAngleShooting>().bullet;
  }

  // Update is called once per frame
  void Update () {
    if (Input.GetButton("Fire1")&& Time.realtimeSinceStartup - fireTime >= fireInterval) {
      fireTime = Time.realtimeSinceStartup;
      foreach(BodyShotPair gO in partsWithGuns)
      {
        if(gO.shotType == "EnemyFixedAngleShooting")
        {
          gO.thisGameObject.GetComponent<EnemyFixedAngleShooting>().PlayerUsingShots();
        }
      }
    }

  }

  private void OnTriggerEnter(Collider other)
  {
    if (other.gameObject.tag == "Wreck")
    {
      other.gameObject.transform.SetParent(this.transform);
      if (other.GetComponent<EnemyFixedAngleShooting>()) {
        connectThing(other.gameObject);
      }
      if (other.GetComponent<EnemyDestruction>()) {
        other.GetComponent<EnemyDestruction>().rotate = false;
      }
    }
  }

  public void connectThing(GameObject other) {
    BodyShotPair bSP = new BodyShotPair();
    bSP.thisGameObject = other;
    bSP.thisGameObject.AddComponent<CanTakeDamage>();
    bSP.shotType = "EnemyFixedAngleShooting";
    bSP.thisGameObject.GetComponent<EnemyFixedAngleShooting>().enabled = true;
    bSP.thisGameObject.GetComponent<EnemyFixedAngleShooting> ().bullet = friendlyBullet;
    partsWithGuns.Add(bSP);
  }
}

[System.Serializable]
struct BodyShotPair
{
  public GameObject thisGameObject;
  public string shotType;
}
