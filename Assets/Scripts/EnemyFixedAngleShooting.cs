﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFixedAngleShooting : MonoBehaviour {

  public GameObject bullet;
  private float fireTime = 0;
  public float fireInterval;

  public float badBulletLifetime;
  public bool dead;


  // Use this for initialization
  void Start () {
  }

  // Update is called once per frame
  void Update () {
    if (Time.realtimeSinceStartup - fireTime >= fireInterval && !dead) {
      fireTime = Time.realtimeSinceStartup;
      GameObject badBullet = GameObject.Instantiate(bullet, transform.position, transform.rotation);
      Destroy (badBullet, badBulletLifetime);
    }
  }

  public void PlayerUsingShots()
  {
    if (Time.realtimeSinceStartup - fireTime >= fireInterval)
    {
      fireTime = Time.realtimeSinceStartup;
      GameObject badBullet = GameObject.Instantiate(bullet, transform.position, transform.rotation);
      Destroy(badBullet, badBulletLifetime);
    }
  }
}
