﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletDestruction : MonoBehaviour {

  public float timeToDeath = 10;

  // Use this for initialization
  void Start () {

  }

  private void Update()
  {
    timeToDeath -= Time.deltaTime;
    if(timeToDeath <= 0)
    {
      Destroy(gameObject);
    }
  }

  // Update is called once per frame
  void OnTriggerEnter (Collider other)
  {
    if(other.gameObject.tag == "Player") {
      Destroy(gameObject);
    }
  }
}
