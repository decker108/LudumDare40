﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanTakeDamage : MonoBehaviour {
  private Player player;
  void OnTriggerEnter(Collider other)
  {
    if(player == null) {
      player = GameObject.FindGameObjectsWithTag("Player")[0].GetComponent<Player>();
    }
    if (!player.IsInvincible()) {
      if (other.gameObject.tag == "Enemy") {
        player.TakeDamage();
      }
    }
  }
}
