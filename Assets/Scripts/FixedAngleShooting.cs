﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixedAngleShooting : MonoBehaviour {

	public GameObject bullet;
	private float fireTime = 0;
	public float fireInterval;
	private AudioSource ljud;

	public float bulletLifetime;


	// Use this for initialization
	void Start () {
		ljud = this.gameObject.GetComponent<AudioSource> ();
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetButton("Fire1")&& Time.realtimeSinceStartup - fireTime >= fireInterval) {
			fireTime = Time.realtimeSinceStartup;
			GameObject newBullet = GameObject.Instantiate(bullet, transform.position, transform.rotation);
			ljud.Play();
			Destroy (newBullet, bulletLifetime);
		}
	}
}
