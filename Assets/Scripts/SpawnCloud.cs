﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnCloud : MonoBehaviour {

public Transform from;  
public Transform to;  
    public int numberOfClouds = 5;
    public int spawnRange = 50;
    public int spawnRangeToNext = 20;	
    public List<GameObject> cloudSets;

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
			//gör en for loop för hur många waves som ska spawna och sätt dem +x ifrån varandra.
			for(int i = 0; i <= numberOfClouds; i++)
			{
				int which = Random.Range(0,cloudSets.Count);
				GameObject cloud = GameObject.Instantiate(cloudSets[which], other.transform.position, Quaternion.AngleAxis(Random.Range(0,359), Vector3.up));
                float randY = (Random.value - 0.5f) * System.Math.Abs(Vector3.Distance(from.position, to.position));
				cloud.transform.position += new Vector3(randY, 0, spawnRange + (i * spawnRangeToNext));
			}

            GetComponent<Collider>().enabled = false;
        }
    }
}
