﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainHandler : MonoBehaviour {

    public float scrollSpeed = 5;
	public GameObject gameOverPanel;
	public int enemyKillCount;
	public Text killScoreText;
	public int civCashCount;
	public int numberOfWaves = 0;
	float counter = 15;
	float ogCounter;
	GameObject dialogue;


	// Use this for initialization
	void Start () {
		ogCounter = counter;
		dialogue = GameObject.FindGameObjectWithTag ("Dialogue");
	}

	// Update is called once per frame
	void Update () {
		counter -= Time.deltaTime;
		if (counter <= 0)
		{
			numberOfWaves++;
			Debug.Log ("BEGIN WAVE " + numberOfWaves);
			counter = ogCounter;
		}
		if(civCashCount >= 1 && dialogue.GetComponent<DialogScript>().sendMessageAfter10Kills)
		{ 
			dialogue.GetComponent<DialogScript>().gameObject.SetActive(true);
			dialogue.GetComponent<DialogScript>().FirstMessage ();
			dialogue.GetComponent<DialogScript>().sendMessageAfter10Kills = false;
		}
		if (civCashCount >= 10 && dialogue.GetComponent<DialogScript>().sendMessageAfter30Kills) 
		{
			dialogue.GetComponent<DialogScript>().SecondMessage ();
			dialogue.GetComponent<DialogScript>().sendMessageAfter30Kills = false;
		}
		if (civCashCount >= 30 && dialogue.GetComponent<DialogScript>().sendMessageAfter50Kills) 
		{
			dialogue.GetComponent<DialogScript>().ThirdMessage ();
			dialogue.GetComponent<DialogScript>().sendMessageAfter50Kills = false;
		}
		if (civCashCount >= 100 && dialogue.GetComponent<DialogScript>().sendMessageAfter100Kills) 
		{
			dialogue.GetComponent<DialogScript>().FourthMessage ();
			dialogue.GetComponent<DialogScript>().sendMessageAfter100Kills = false;
		}
	}

	public void LoadGameOverPanel(){
		Debug.Log ("inside LoadGameOverPanel");
		PauseGame ();
		killScoreText.text = "Enemies killed: " + enemyKillCount;
		Debug.Log ("end of LoadGameOverPanel");
	}

	public void RestartGame()
	{
		Debug.Log ("inside REstartGame");
		Time.timeScale = 1;
		SceneManager.LoadScene(SceneManager.GetActiveScene().name, LoadSceneMode.Single);
	}

	public void QuitGame()
	{
		Debug.Log ("inside QuitGAme");
		Application.Quit();
	}

	private void PauseGame(){
		Time.timeScale = 0f;
		gameOverPanel.SetActive (true);
	}

	public int getCivCashCount() {
		Debug.Log ("civCashCount " + civCashCount);
		return civCashCount;
	}

}
	
