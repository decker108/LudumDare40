﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissilePowerUpCollision : MonoBehaviour {

//	private AudioSource ljud;

	void Start () {
//		ljud = this.gameObject.GetComponent<AudioSource> ();
	}
	
	void Update () {
		
	}

	void OnTriggerEnter(Collider other) {
		if (other.tag.Equals ("Player")) {
			MouseBasedShooting mouseBasedShooting = other.gameObject.GetComponent<MouseBasedShooting> ();
			mouseBasedShooting.enabled = true;
//			ljud.Play();
			Destroy (gameObject);
		}
	}
}
