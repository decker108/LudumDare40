﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KillCiv : MonoBehaviour {

    MainHandler SecretObject;
	Image CasualtyBar;

    // Use this for initialization
    void Start()
    {
        SecretObject = GameObject.FindGameObjectWithTag("MainHandler").GetComponent<MainHandler>();
		CasualtyBar = GameObject.FindGameObjectWithTag ("CasualtyBar").GetComponent<Image> ();
    }

    // Update is called once per frame
    void Update () {
		
	}

    public void OnTriggerEnter(Collider other)
    {
		if (other.gameObject.tag == "Bullet")
        {
			SecretObject.civCashCount++;
			Destroy (other.gameObject);
            Destroy(gameObject);

			CasualtyBar.fillAmount = (float)SecretObject.GetComponent<MainHandler> ().getCivCashCount() / 100;
        }
    }
}
