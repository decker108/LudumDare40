﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogScript : MonoBehaviour {

  Text dialog;
  GameObject angryMan;
  MainHandler mH;
  public bool sendMessageAfter10Kills = true;
  public bool sendMessageAfter30Kills = true;
  public bool sendMessageAfter50Kills = true;
  public bool sendMessageAfter100Kills = true;

  // Use this for initialization
  void Start () {
    angryMan = GameObject.Find("Angry Man");
    dialog = transform.GetComponent<Text>();
    StartCoroutine(DeactivateAfterWait("Yer mission is to shoot stuff up. And no civilian casualties this time, ya hear me!?", 7));
    mH = GameObject.FindGameObjectWithTag("MainHandler").GetComponent<MainHandler>();
  }

  IEnumerator DeactivateAfterWait(string msg, int waitTimeSeconds)
  {
    dialog.gameObject.transform.parent.gameObject.SetActive(true);
    SetVisible(true);
    dialog.text = msg;
    yield return new WaitForSeconds(waitTimeSeconds);
    dialog.gameObject.transform.parent.gameObject.SetActive(false);
    SetVisible(false);
  }

  public void FirstMessage()
  {
    Debug.Log("FISAHFLSIDJHAILS");
    SetVisible(true);
    dialog.gameObject.SetActive(true);
    dialog.gameObject.transform.parent.gameObject.SetActive(true);
    sendMessageAfter10Kills = false;
    StartCoroutine(DeactivateAfterWait("Listen kid! You're our star and we can't have you mess up like this... You've killed 20 of our own already, ya hear me!? Huh!?", 10));
  }

  public void SecondMessage()
  {
    dialog.gameObject.transform.parent.gameObject.SetActive(true);
    sendMessageAfter30Kills = false;
    StartCoroutine(DeactivateAfterWait("THATS IT! I hate you. Your mom hates you. Your country hates you! For the love of GOD!! ABORT YOU SUNUVA..", 10));
  }

  public void ThirdMessage()
  {
    dialog.gameObject.transform.parent.gameObject.SetActive(true);
    sendMessageAfter50Kills = false;
    StartCoroutine(DeactivateAfterWait("Kid, I don't know what ya have against this country, but ya need to stop killin' everyone!", 10));
  }

  public void FourthMessage()
  {
    dialog.gameObject.transform.parent.gameObject.SetActive(true);
    sendMessageAfter100Kills = false;
    StartCoroutine(DeactivateAfterWait("WE GIVE UP! Okay, ya won, please tell us your demands, while there is still anyone left to listen...*CHHSHH*", 10));
  }

  public void SetVisible(bool isVisible) {
    Color c = dialog.color;
    Color n = new Color(c.r, c.g, c.b, isVisible ? 1.0f : 0.0f);
    dialog.color = n;
    SpriteRenderer spr = angryMan.GetComponentInChildren<SpriteRenderer>();
    Color c1 = spr.color;
    Color n1 = new Color(c1.r, c1.g, c1.b, isVisible ? 1.0f : 0.0f);
    spr.color = n1;
  }
}
